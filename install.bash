#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SNAPPY_PATH="$SCRIPT_DIR/snappy/"

if [ ! -d "snappy_src" ]; then
  git submodule init
  git submodule update --recursive

  cd snappy_src
  bash ./autogen.sh
  bash ./autogen.sh
  ./configure --prefix="$SCRIPT_DIR/snappy/"
  make -j`nproc`
  make install
  cd ..
fi

function install_python_snappy {
  pip install python-snappy --global-option=build_ext --global-option="-I$SNAPPY_PATH/include/" --global-option="-L$SNAPPY_PATH/lib/"
}

function python2_setup {
  if [ ! -d ".venv2" ]; then
    virtualenv .venv2
  fi
  source .venv2/bin/activate
  install_python_snappy
  pip install -r requirements.txt
  deactivate
}

function python3_setup {
  if [ ! -d ".venv3" ]; then
    virtualenv .venv3
  fi
  source .venv3/bin/activate
  install_python_snappy
  pip install -r requirements.txt
  deactivate
}

function check_python_version {
  python -c "import sys; print(sys.version_info[0])"
}

PYTHON_VERSION=$(check_python_version)

if [ "$PYTHON_VERSION" = "2" ]; then
  python2_setup
else
  python3_setup
fi

