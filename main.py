from __future__ import division

from contextlib import contextmanager
from functools import partial
from itertools import *
import os
import timeit
import pandas as pd

from generators import *

import snappy
import zlib
import bz2

# Parameters
benchmark_repeat_times = 10
benchmark_cases = {
    'random_string': True,
    'random_binary': True,
    'random_image': True,
    'realworld_text': True,
    'realworld_image': True,
}

class Zlib_algo(object):
    def __init__(self, compress_kwargs=None, decompress_kwargs=None):
        if compress_kwargs is None:
            compress_kwargs = {}
        if decompress_kwargs is None:
            decompress_kwargs = {}
        self.compress_kwargs = compress_kwargs
        self.decompress_kwargs = decompress_kwargs

    def compress(self, mem):
        return zlib.compress(mem, self.compress_kwargs.get('level', 6))

    def decompress(self, mem):
        return zlib.decompress(mem, self.decompress_kwargs.get('wbits', 15),
                               self.decompress_kwargs.get('bufsize', 16384))

class Bz2_algo(object):
    def __init__(self, compress_kwargs=None, decompress_kwargs=None):
        if compress_kwargs is None:
            compress_kwargs = {}
        if decompress_kwargs is None:
            decompress_kwargs = {}
        self.compress_kwargs = compress_kwargs
        self.decompress_kwargs = decompress_kwargs

    def compress(self, mem):
        return bz2.compress(mem, self.compress_kwargs.get('compresslevel', 9))

    def decompress(self, mem):
        return bz2.compress(mem)

class Snappy_algo(object):
    def __init__(self, compress_kwargs=None, decompress_kwargs=None):
        if compress_kwargs is None:
            compress_kwargs = {}
        if decompress_kwargs is None:
            decompress_kwargs = {}
        self.compress_kwargs = compress_kwargs
        self.decompress_kwargs = decompress_kwargs

    def compress(self, mem):
        return snappy.compress(mem)

    def decompress(self, mem):
        return snappy.compress(mem)

def benchmark_algo_one(algo, mem, reptimes):
    sum = [0,0,0]
    for i in range(reptimes):
        start_time = timeit.default_timer()
        mem_compressed = algo.compress(mem)
        compression_time = timeit.default_timer() - start_time
        sum[0] += compression_time

        compression_ratio = len(mem_compressed) / len(mem)
        sum[1] += compression_ratio

        start_time = timeit.default_timer()
        algo.decompress(mem_compressed)
        decompression_time = timeit.default_timer() - start_time
        sum[2] += decompression_time

    mean = (i/reptimes for i in sum)
    return mean

if __name__ == '__main__':
    # Compression Algorithms to benchmark
    algorithms = []
    algo_name        = ['zlib',      'bz2']
    algo_classes     = [Zlib_algo,   Bz2_algo]
    algo_level_name  = ['level',     'compresslevel']
    algo_level_avail = [range(0, 10), range(1, 10)]
    for name, algo, level_name, levels in \
        zip(algo_name, algo_classes, algo_level_name, algo_level_avail):
        for level in levels:
            algorithms.append(('{}({})'.format(name, level), algo({level_name: level})))

    algorithms.append(('snappy', Snappy_algo()))

    # Input Data Generators
    generators = []

    # Generated Strings
    if benchmark_cases.get('random_string', False):
        sizes = [64,128,1024,5000,10000,50000]
        #sizes = sizes[:4]
        params_lst = [(i,) for i in sizes]
        for params in params_lst:
            generators.append(('random_string({})'.format(*params), partial(random_string, *params)))

    # Generated Binaries
    if benchmark_cases.get('random_binary', False):
        sizes = [
            64,128,1024,
            5*1024, # 5KB
            100*1024, # 100KB
            5*1024*1024, # 5MB
        ]
        sizes = sizes[:4]
        params_lst = [(i,) for i in sizes]
        for params in params_lst:
            generators.append(('random_binary({})'.format(*params),
                               partial(random_binary, *params)))

    # Generated Images
    if benchmark_cases.get('random_image', False):
        image_formats = ['jpeg', 'png', 'bmp', 'gif']
        sizes = [(100,100), (200,200), (640,480), (1024,768), (2560,1440), (4000,3000)]
        #sizes = sizes[:3]
        params_lst = product(image_formats, sizes)
        for params in params_lst:
            generators.append(('random_image({}, {})'.format(*params), partial(random_image, *params)))


    # Generator of Real-world Texts
    if benchmark_cases.get('realworld_text', False):
        txt_file_dir = 'data/text/'
        txt_files = ['hongloumeng.txt',
                     'jiangye.txt',
                     'scrooge.txt',
                     'shakespeare.txt']
        for txt in txt_files:
            generators.append((txt, partial(from_text_file, os.path.join(txt_file_dir, txt))))

    # Generator of Real-world Images
    if benchmark_cases.get('realworld_image', False):
        img_file_dir = 'data/image/'
        img_files = ['100x100_1.jpg',   '100x100_2.jpg',
                     '400x300_1.jpg',   '400x300_2.jpg',
                     '1024x768_1.jpg',  '1024x768_2.jpg',
                     '2560x1440_1.jpg', '2560x1440_2.jpg',
                     '4000x3000_1.jpg', '4000x3000_2.jpg']
        #img_files = img_files[:4]
        image_formats = ['jpeg', 'png', 'bmp', 'gif']
        params_lst = [('image({}, {})'.format(fmt, img), fmt, os.path.join(img_file_dir, img))
                      for fmt in image_formats for img in img_files]
        for params in params_lst:
            generators.append((params[0], partial(from_image_file, *params[1:])))

    # Generate Summary
    major_axis = [gen_name for gen_name, gen in generators]
    minor_axis = [algo_name for algo_name, algo in algorithms]
    summary = pd.DataFrame(index=major_axis, columns=minor_axis)

    for gen_name, gen in generators:
        mem = gen()
        for algo_name, algo in algorithms:
            print('Running: {}/{}'.format(gen_name, algo_name))
            ctime, cratio, dtime = benchmark_algo_one(algo, mem, benchmark_repeat_times)
            summary.loc[gen_name, algo_name] = (ctime, cratio, dtime)

    summary.to_hdf('compression_benchmark.h5', 'summary')
