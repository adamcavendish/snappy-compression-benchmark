import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

summary = pd.read_hdf('compression_benchmark.h5')

def str_report_one(tpl):
    ctime, cratio, dtime = tpl
    return '{:8.3f}ms {:6.2f}% {:8.3f}ms' \
        .format(ctime, cratio, dtime)

def normalize(tpl):
    ctime, cratio, dtime = tpl
    return (ctime*1000, cratio*100, dtime*1000)

summary_norm = summary.apply(lambda col: col.apply(normalize))
summary_report = summary_norm.apply(lambda col: col.apply(str_report_one))

# Print the summary
for gen_name, gen_summary in summary_report.iterrows():
    print(gen_name)
    print('\n'.join(str(gen_summary).split('\n')[:-1]))
    print()

nrows, ncols = summary_norm.shape
# Ratio vs. Compression
magma = plt.get_cmap('viridis')
magma_c = np.linspace(0, 1, ncols)
for i in range(nrows):
    figure = plt.figure()
    subplt = plt.subplot(111)
    gen_name = summary_norm.index[i]

    ctimes = []
    cratios = []
    for j in range(ncols):
        algo_name = summary_norm.columns[j]
        ctime, cratio, _ = summary_norm.iloc[i, j]

        color = magma(magma_c[j])
        subplt.scatter(cratio, ctime, marker='o', s=30, color=color, label=algo_name)

        ctimes.append(ctime)
        cratios.append(cratio)

    box = subplt.get_position()
    subplt.set_position([0.125, 0.125, 0.65, 0.8])
    #subplt.set_position([0.125, 0.25, 0.65, 0.7])
    plt.title(gen_name)
    subplt.set_title(gen_name)
    subplt.set_xlim((-5, np.max(cratios)+10))
    subplt.set_xlabel('compression ratio (%)', fontsize=16)
    subplt.set_ylabel('compression time (ms)', fontsize=16)
    subplt.legend(scatterpoints=1, loc='upper left', bbox_to_anchor=(1, 1.02),
                  ncol=1, fancybox=True, shadow=True, prop={'size': 11})
    plt.savefig('output/' + gen_name + '.jpg')
    #plt.show()
    plt.close(figure)
