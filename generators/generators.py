import sys
import numpy as np
from PIL import Image

if sys.version_info < (3, 0):
    from StringIO import StringIO
    SIO = StringIO
else:
    from io import BytesIO
    SIO = BytesIO

def random_string(size, dictionary=None):
    '''
    Parameter:
    - size: random_string's size
    - dictionary: the random string's dictionary, default is all printable characters from 0x20 to 0x7E
    '''
    if dictionary is None:
        dictionary = list(range(0x20, 0x7E))
    ords = np.random.randint(0, len(dictionary), size)
    return np.char.mod('%c', ords)

def random_binary(size, dictionary=None):
    '''
    Parameter:
    - size: random_string's size
    - dictionary: the random string's dictionary, default is all characters from 0x00 to 0xFF
    '''
    if dictionary is None:
        dictionary = list(range(0x00, 0xFF))
    ords = np.random.randint(0, len(dictionary), size)
    return np.char.mod('%c', ords)

def random_image(img_fmt, size, channels=3):
    '''
    Parameter:
    - img_fmt: @ref: http://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html
    - size: the image size in (row, col)
    - channels: the image channel (1,3,4 supported, default=3)
    '''
    if channels not in [1,3,4]:
        raise RuntimeError('Not supported channels (1,3,4 supported)')
    channel_map = ['L', None, 'RGB', 'RGBA']
    nrows, ncols = size
    imarray = np.random.rand(nrows, ncols, channels)

    sio = SIO()
    im = Image.fromarray(imarray.astype('uint8')).convert(channel_map[channels])
    im.save(sio, img_fmt)
    ret = sio.getvalue()
    sio.close()
    return ret

def from_text_file(filepath):
    '''
    Read the text file in as mem
    '''
    with open(filepath, 'rb') as f:
        return f.read()


def from_image_file(to_format, filepath, channels=3):
    '''
    Parameter:
    - to_format: the image read in will be converted into such image format
    - filepath: the image filepath to read in
    - channels: the image channel (1,3,4 supported, default=3)
    '''
    if channels not in [1,3,4]:
        raise RuntimeError('Not supported channels (1,3,4 supported)')
    channel_map = ['L', None, 'RGB', 'RGBA']
    im = Image.open(filepath).convert(channel_map[channels])
    sio = SIO()
    im.save(sio, to_format)
    ret = sio.getvalue()
    sio.close()
    return ret
